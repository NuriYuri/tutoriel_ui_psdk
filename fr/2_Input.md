# Input
Quand vous créez une interface à l'aide de `GamePlay::Base` il arrive assez vite que vous ayez besoin de gérer les entrées utilisateur.

Sous PSDK il existe un module qui permet de lire les entrées utilisateur : le module [Input](https://psdk.pokemonworkshop.fr/litergss/LiteRGSS/Input.html).

## Les touches supportées par le module Input

Le module Input permet par défaut de lire 17 touches virtuelles nommés par des symboles. Ces touches virtuelles sont à entrer en paramètre des différentes fonctions du module Input pour détecter l'appuis d'une touche.

Voici la liste de ces touches ainsi que leur assignement par défaut :

* `:A` (validation), activée par C, Entrée, Espace, la touche A de la manette 1 XBOX 360.
* `:B` (annulation), activée par X, Shift, Retour, Echap, la touche B de la manette 1 XBOX 360.
* `:X` (Menu), activée par V, Alt Gauche, la touche X de la manette 1 XBOX 360.
* `:Y` (Raccourcis), activée par W, Alt Droit, la touche Y de la manette 1 XBOX 360.
* `:L` activée par A, la touche L de la manette 1 XBOX 360.
* `:R` activée par E, la touche R de la manette 1 XBOX 360.
* `:L2` activée par la touche Num1.
* `:R2` activée par la touche Num3.
* `:L3` non activée actuellement.
* `:R3` non activée actuellement.
* `:START` activée par B, la touche Start de la manette 1 XBOX 360.
* `:SELECT` activée par N, la touche retour de la manette 1 XBOX 360.
* `:HOME` activée par la touche CTRL.
* `:UP` activée par haut, NumPad8 et Z.
* `:DOWN` activée par bas, NumPad2 et S.
* `:LEFT` activée par gauche, NumPad4 et Q
* `:RIGHT` activée par droit, NumPad6 et D

Il est possible de modifier le binding de ces touches en accédant à `Input::Keys`. Cette variable est un Hash associant chacune des touches à un tableau d'entiers provenants d'une des constantes du module `Input::Keyboard` ou de la formule `-32 * joy_id - button_id - 1` où 
* `joy_id` est le numéro de manette allant de 0 à n
* `button_id` est le numéro de touche allant de 0 à 31.

## Détecter quand une touche est actionnée

Dans le module Input, il existe trois types d'actions :

* Touche maintenue `Input.press?(key)`
: Quand l'utilisateur a appuyé sur la touche et maintien la touche à sa position appuyée.

* Touche appuyée `Input.trigger?(key)`
: Quand l'utilisateur vient d'appuyer sur la touche.

* Touche relâchée `Input.released?(key)`
: Quand l'utilisateur appuyait sur la touche et qu'il vient d'arrêter d'appuyer dessus.

Par exemple, si vous voulez savoir si le joueur a appuyé sur la touche d'action (`:A`) vous écrirez cette condition :

```ruby
if Input.trigger?(:A)
  # Le joueur a appuyé sur la touche action
end
```

## Input.repeat?

Dans le module Input il existe une détection un peu particulière : `Input.repeat?(key)`

Cette détection vous permet d'explorer des listes de manière convenable car c'est un mélange entre `Input.trigger?` et `Input.press?` sans que vous ne vous retrouviez en bas de la liste en moins d'une seconde.

Exemple :

```ruby
if Input.repeat?(:LEFT)
  # Entrée dans la condition de manière periodique si le joueur reste appuyé sur gauche.
end
```

## Retour à GamePlay::Base

Comme il peut arriver assez souvent de réaliser des listes ou des pseudo listes, `GamePlay::Base` a prévu deux fonctions vous permettant de gérer une "liste".

Souvent une liste est décrit par une variable servant d'index `@index` (position courante dans la liste) et d'une vue graphique mise à jour à chaque fois que l'index change.

Dans initialize vous mettrez la variable `@index` à une valeur initiale et dans `update` vous détecterez les changements de cette variable. 

La méthode utilisée dans `GamePlay::Base` est `index_changed`. 
* Le premier argument est le nom de la variable d'index (ici `:@index`)
* Le deuxième argument est la touche utilisée pour retirer 1 à la variable d'index
* Le troisième argument est la touche utilisée pour ajouter 1 à la variable d'index
* Le quatrième argument est la valeur max de la variable d'index
* Le dernier argument (optionel) est la valeur min de la variable d'index

Exemple, une variable d'index `@index` variant de 0 à 5 et étant modifié par les touches haut et bas.

```ruby
if index_changed(:@index, :UP, :DOWN, 5)
  # Ici @index a changé de valeur
end
```

Notez que cette fonction repose sur `Input.repeat?` et que si vous êtes à la fin de la liste ça retourne au début de la liste.

Si vous voulez rester bloqué en haut ou en bas de la liste utilisez `index_changed!` à la place.