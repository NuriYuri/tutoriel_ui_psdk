# SpriteStack
Les `SpriteStack` dans PSDK sont des objets très pratiques, ils permettent d'afficher des informations dynamiques comme le Sprite d'un Pokémon ou des textes. Ce dynamisme ce fait grâce à une propriété `data` de certains sprites et objet de texte, les `SpriteStack` possèdent également cette propriété et la transmet à tout son contenu.

## Créer un SpriteStack

Vous avez deux solutions pour créer un `SpriteStack` :

- Créer un objet `SpriteStack` et le stocker dans une variable. (méthode 1)
- Créer une classe héritant de `SpriteStack` et instancier un objet de cette classe. (méthode 2)

### Méthode 1

```ruby
stack = UI::SpriteStack.new(viewport, x, y)
```

Ceci crée un `SpriteStack` dans le viewport indiqué (la propriété viewport sera transmise à tous les sprites/textes du stack) en commençant à la coordonnée x, y.

Ceci veut dire que quand vous voudrez ajouter un sprite ou un texte généré par le `SpriteStack` à une coordonnée 15, 30; il sera crée à la coordonnée 15 + x, 30 + y.

Notez que vous pouvez spécifier le cache utilisé par le `SpriteStack` pour la création des sprites :
```ruby
stack = UI::SpriteStack.new(viewport, x, y, default_cache: :picture)
```
Au lieu de prendre dans le cache `:interface` (par défaut) ça ira prendre dans le cache `:picture`.

### Méthode 2

Définition de la classe :

```ruby
class MonStackPerso < UI::SpriteStack
  # Create a new MonStackPerso
  # @param viewport [Viewport]
  def initialize(viewport)
    super(viewport, 64, 128, default_cache: :icon)
    # perform push / add_text here
  end
end
```

Instanciation :

```ruby
stack = MonStackPerso.new(viewport)
```

L'avantage de cette façon de faire est qu'il est beaucoup plus facile de maintenir le code puis-ce que la description du contenu se fait dans la classe `MonStackPerso`. Tout ce que vous avez à faire avec `stack` c'est de lui affecter sa propriété `data` et éventuellement de le déplacer si besoin.

## Propriétés similaires aux Sprites

Les `SpriteStack` on ces propriétés/méthodes en commun avec les sprites :

- x : Position x du stack.
- y : Position y du stack.
- set_position : Définie la position x/y du stack (et déplace les Sprites en conséquence).
- visible : Visibilité des sprites à l'intérieur du stack.
- simple_mouse_in? : Détecte si la souris est dans le premier sprite du stack.
- translate_mouse_coords : Traduit les coordonnées de la souris en coordonnées dans le premier sprite du stack.
- dispose : Libère tous les sprites du stack
- update : Met à jour le processus interne des sprites (et du stack).

## Ajouter un Sprite

Pour ajouter un sprite, vous utilisez la méthode `push` :

```ruby
sprite = stack.push(x, y, filename, *args, rect: nil, ox: 0, oy: 0, type: Sprite)
```

Tous les paramètres à partir de args (inclu) sont optionnels. Vous pouvez mettre filename à `nil` si vous ne voulez pas définir l'image tout de suite. 

Le paramètre `type` vous permet de choisir un autre type de Sprite et le paramètre `*args` vous permet de définir les arguments d'instanciation mis après `viewport` pour le sprite.

Exemple, afficher l'icone de la Master Ball en coordonée 50, 30 :

```ruby
stack.psuh(50, 30, '001')
```

Exemple, afficher le sprite d'un Pokémon :

```ruby
stack.push(0, 0, nil, type: UI::PokemonFaceSprite)
# plus loin dans le code
stack.data = PFM::Pokemon.new(151, 5)
```

Vous devriez voir un Mew.

**Note :** Si vous avez crée un stack par la méthode 2, vous devez omettre le `stack.` dans vos appels.

## Afficher du texte

Pour afficher du texte, vous utilisez la méthode `add_text`. La font utilisé par défaut est la 0, si vous voulez changer ça, utilisez la méthode 2 et mettez la variable `@font_id` à la valeur souhaité. Il est déconseillé d'avoir plusieurs font dans le même stack.

```ruby
text = stack.add_text(x, y, width, line_height, str, align = 0, outlinesize = Text::Util::DEFAULT_OUTLINE_SIZE, type: Text, color: 0)
```
Description des paramètres :
- `x`
- `y`
- `width`
- `line_height`
- `str`
- `align`
- `outlinesize`
- `color`
- `type`

Exemple, afficher un texte statique centré :

```ruby
stack.add_text(0, 0, 320, 16, 'Statique...', 1)
```

Exemple, afficher le nom d'un Pokémon :

```ruby
stack.add_text(0, 0, 80, 16, :name, type: UI::SymText)
# plus loin dans le code
stack.data = PFM::Pokemon.new(25, 5)
```

Ca devrait afficher "Pikachu".

Notez que le paramètre `str` a été remplacé par `:name` qui fait référence à la propriété `name` de l'objet envoyé en `data`.

## Lier des SpriteStack

La liaison de SpriteStack est une façon de définir vos `SpriteStack` de sorte à ce que quand vous envoyez une donnée `data` au `SpriteStack` parent, les `SpriteStack` liés reçoivent une propriété de cette donnée.

Ainsi, vous pouvez réaliser une interface d'apprentissage d'attaque qui affiche les caractéristique du Pokémon et ses 4 attaques avec les détails et ce, sans avoir à définir toute les affectations dans la scène. La scène n'enverra que le Pokémon au `SpriteStack` parent et celui-ci se chargera d'envoyer les bonnes infos aux `SpriteStack` liés.

Pour réaliser la liaison, vous avez besoin de modifier la propriété `data=` du `SpriteStack` parent, voici un exemple :

```ruby
module UI
  # On se met dans le module UI pour accéder à tous les Sprites pratiques sans avoir à écrire UI::
  class ApprentissageAttaque_Main < SpriteStack
    # Crée une UI ApprentissageAttaque
    # @param viewport [Viewport] 
    def initialize(viewport)
      super(viewport)
      push(xxx, yyy, nil, type: PokemonFaceSprite)
      push(xxx, yyy, nil, type: Type1Sprite)
      push(xxx, yyy, nil, type: Type2Sprite)
      add_text(xxx, yyy, www, 16, :given_name, 1, type: SymText)
      # etc... pour les infos du Pokémon
      @attaques = Array.new(4) { |i| ApprentissageAttaque_Attaque.new(viewport, i) }
    end

    # Modifie le Pokémon affiché par l'interface
    # @param pokemon [PFM::Pokemon, nil] nil rend l'interface invisible
    def data=(pokemon)
      # On appel l'original pour les sprites définis dans ce stack
      super
      # On envoie les propriétés au stack liés
      @attaques.each_with_index do |attaque_stack, index|
        attaque = pokemon ? pokemon.skills_set[index] : nil
        attaque_stack.data = attaque
      end
    end
  end
end
```

## Mot de la fin

Avec ces informations, vous devriez avoir de quoi facilement définir vos interfaces. Je vous conseil de regarder la documentation du module UI (sur le site ou dans les scripts) pour avoir plus d'informations sur les différentes classes qui existent pour afficher vos éléments d'interface à l'aide de `SpriteStack`.
