# Sprite

Les sprites permettent d'afficher une image ou une portion d'image à l'écran.

## Créer un Sprite

Un Sprite se crée généralement dans un Viewport, le Viewport est une surface de l'écran dans laquelle les Sprites, Window et Textes s'affichent.

Pour créer un sprite on utilise la méthode `new` de la classe Sprite :
```ruby
sprite = Sprite.new(viewport)
```

Pour la plupart des sprites, le viewport est le premier argument; ceci permet de les utiliser simplement avec la classe SpriteStack. 

## Indiquer l'image à afficher

Pour indiquer l'image à afficher vous avez deux solutions :
- La propriété `bitmap` que vous affecterez à un objet `Bitmap` (par exemple le bitmap d'un autre Sprite).
- La méthode `set_bitmap` qui vous permet de spécifier une image provenant de l'un des différents caches.

Exemple, afficher l'image 001 de `RPG::Cache.icon`
```ruby
sprite.set_bitmap('001', :icon)
```

Les différents caches sont :
- `:animation` : Image provenant de `Graphics/Animations`
- `:autotile` : Image provenant de `Graphics/Autotiles`
- `:ball` : Image provenant de `Graphics/Ball`
- `:battleback` : Image provenant de `Graphics/BattleBacks`
- `:battler` : Image provenant de `Graphics/Battlers`
- `:character` : Image provenant de `Graphics/Characters`
- `:fog` : Image provenant de `Graphics/Fogs`
- `:icon` : Image provnant de `Graphics/Icons`
- `:interface` : Image provenant de `Graphics/Interface`
- `:panorama` : Image provenant de `Graphics/Panoramas`
- `:particle` : Image provenant de `Graphics/Particles`
- `:pc` : Image provenant de `Graphics/PC`
- `:picture` : Image provenant de `Graphics/Pictures`
- `:pokedex` : Image provenant de `Graphics/Pokedex` (Attention, les sous dossier de Graphics/Pokedex sont pas compilés !)
- `:title` : Image provenant de `Graphics/Titles`
- `:tileset` : Image provenant de `Graphics/Tilesets`
- `:transition` : Image provenant de `Graphics/Transitions`
- `:windowskin` : Image provenant de `Graphics/Windowskins`
- `:foot_print` : Image provenant de `Graphics/Pokedex/FootPrints`
- `:b_icon` : Image provenant de `Graphics/Pokedex/PokeIcon`

Pour les fronts et back des Pokémon il est recommandé d'utiliser les fonctions de la classe `PFM::Pokemon` qui renvoie le bitmap associé au Pokémon en question.

## Définir la position du sprite

Les sprites ont une position x et y dans le viewport, cette position permet d'indiquer où le pixel d'origine de l'image s'affiche.

Pour définir la position vous avez deux moyens :
- En affectant la propriété `x` ou `y` du sprite.
- En utilisant la méthode `set_position`.

Exemple : Afficher le sprite en x = 32, y = 85
```ruby
sprite.set_position(32, 85)
```

## Définir le pixel d'origine de l'image

Le pixel d'origine de l'image est assez important, c'est lui qui est toujours affiché à la position x, y du sprite dans le viewport. Tous les autres pixels sont affichés à des positions différentes en fonction du zoom et de l'angle du `Sprite`.

Pour définir le pixel d'origine vous avez deux possibilités :
- Affecter `ox` ou `oy` du sprite.
- Utiliser la méthode `set_origin`.

Exemple : Afficher une image centrée au centre du viewport
```ruby
# On récupère les dimensions de la surface d'affichage
rect = sprite.viewport&.rect || Graphics
# On affiche le sprite au centre de la surface
sprite.set_position(rect.width / 2, rect.height / 2)
# On centre l'image
sprite.set_origin(sprite.width / 2, sprite.height / 2)
```

Note pour ceux qui ont l'habitude d'utiliser `sprite.bitmap.width`, cette façon de faire est erroné. Il faut utiliser `sprite.src_rect.width` car cela correspond à la largeur de la surface de l'image que l'on affiche à l'écran. `sprite.width` est un alias de `sprite.src_rect.width`.

## Définir la surface de l'image à afficher

Il arrive assez souvent qu'une image contienne plusieurs images, soit parce que c'est une animation, soit parce que c'est une `SpriteSheet`, pour définir la surface de l'image à afficher, la classe `Sprite` possède une propriété `src_rect` qui décrit cette surface.

Exemple : afficher la cellule en position 1, 3 dans une sprite_sheet 4x5
```ruby
cell_width = sprite.bitmap.width / 4
cell_height = sprite.bitmap.height / 5
sprite.src_rect.set(cell_width * 1, cell_height * 3, cell_width, cell_height)
```

## Tourner un Sprite

Pour tourner le `Sprite`, vous pouvez accéder à la propriété `angle` qui correspond à l'angle de rotation du sprite en degré (de 0 à 360).

Exemple : tourner le `Sprite` à 45°
```ruby
sprite.angle = 45
```

## Définir le zoom d'un Sprite

Pour définir le zoom d'un `Sprite` vous pouvez utiliser la méthode `zoom=` ou accéder aux propriétés `zoom_x` et `zoom_y`.

## Ordonner les Sprites

En LiteRGSS cette fonctionnalité n'est pas automatique, le meilleur moyen d'ordonner les sprites est de les créer dans le bon ordre (ainsi ils seront naturellement dans le bon ordre d'affichage). Cela dit, si vous avez des sprites qui changent d'ordre d'affichage, vous pouvez utiliser la propriété `z` de votre sprite et invoquer la fonction `sort_z` de son `Viewport` ou de `Graphics` une fois que vous avez affecté la propriété `z` de tous les sprites.

## Mot de la fin

Concernant les sprites basiques, nous avons vu le plus important, d'autres chapitres vont se concentrer sur d'autres types de Sprites ou des outils permettant de simplifier le process.