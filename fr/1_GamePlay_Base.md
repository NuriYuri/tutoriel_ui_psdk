# GamePlay::Base
`GamePlay::Base` est la classe parente de la majorité des interfaces dans PSDK. Cette classe permet plusieurs choses :

* Ne pas avoir à redéfinir la méthode `main` (la transition est gérée pour vous).
* Afficher des messages comme en évènement.
* Appeler ou revenir à une autre interface
* Détecter des changements d'index
* Mettre à jour les boutons de contrôle à la souris

Nous allons voir comment utiliser cette classe pas à pas.

##  Créer une classe héritant de GamePlay::Base

Si vous avez suivit le [tutoriel sur Ruby](https://gitlab.com/NuriYuri/tutoriel_ruby/blob/master/fr/8_Classes_Et_Objets.md#lhéritage), vous devriez savoir comment réaliser une classe qui hérite d'une autre. Pour la simplicité d'écriture, nous alons définir notre classe dans le module GamePlay.

```ruby
module GamePlay
  class MonInterface < Base
    # Create a new MonInterface
    def initialize
      # Indiquer à Base que l'on veut une fenêtre de message en z = 10_000
      super(false, 10_000)
      # On crée le viewport pour notre interface
      @viewport = Viewport.create(:main, 1000)
      # Après cette ligne on crée tous les sprites de l'interface

      # On indique que l'interface est en fonctionnement
      @running = true
    end

    # Update MonInterface
    # @return [Boolean] if the update can continue
    def update
      # Appelez la mise à jour des éléments animés après cette ligne

      # On s'assure de ne pas mettre à jour l'interface si un message est affiché
      return unless super
      # On exécute toute les actions de mise à jour après cette ligne
      
    end
  end
end
```

Dans cette classe nous avons défini seulement deux méthodes :
* initialize : Pour l'initialisation des variables comme `@index` mais aussi pour la création des sprites (éléments d'interface)
* update : Pour la mise à jour (défilement des fonds et détection des entrées de l'utilisateur)

Notez qu'on a pas défini la méthode dispose. En effet, `GamePlay::Base` a une méthode dispose qui appelle la méthode `dispose` de `@viewport` et de la boite de message. Ainsi, vous n'avez pas à vous en charger.

Cela dit, vous aurez probablement besoin d'effacer des éléments vous-même, dans ce cas là définissez la méthode dispose ainsi :
```ruby
    def dispose
      # On appelle la méthode dispose de GamePlay::Base
      super
      # Effacez vos éléments (viewports autres que @viewport) après cette ligne

    end
```

Notez qu'en LiteRGSS effacer un viewport efface les sprites qu'il contient.

## Afficher un message

Pour afficher un message, c'est très simple, vous avez juste besoin d'appeler la méthode `display_message` avec les bons paramètres.

Il existe une fonction similaire mais qui attend la fermeture de la boite de dialogue avant de rendre la main : `display_message_and_wait`.

### Afficher un simple message

Le premier paramètre de `display_message` est le message, pour afficher "Coucou !" on écrira :
```ruby
display_message('Coucou !')
```

### Afficher un message avec un choix

Le deuxième paramètre de `display_message` est le choix initial (de 1 à n, n étant le nombre d'options), les autres arguments sont les choix possibles.

Exemple : choix Oui / Non avec Oui par défaut
```ruby
choix = display_message('Arrêter ?', 1, 'Oui', 'Non')
```
La variable choix vaudra 0 si le joueur choisi Oui, 1 s'il choisi Non. (C'est dû au fonctionnement interne des listes.)

Exemple : choix Oui / Non avec Non par défaut
```ruby
choix = display_message('Arrêter ?', 2, 'Oui', 'Non')
```

## Quitter l'interface

Pour quitter l'interface, deux options s'offre à vous :
* Désigner l'interface vers laquelle vous souhaitez retourner
* Arrêter simplement l'interface

Pour simplement arrêter l'interface, il vous suffit de mettre la variable d'instance `@running` à `false`. Une fois sorti de `update` l'interface se fermera en fondue.

Pour désigner l'interface vers laquelle vous souhaitez retourner, utiliser la méthode `return_to_scene` en précisant la classe de la scène vers laquelle retourner et les arguments au cas où il faudrait la créer.

Exemple : Retourner sur la carte
```ruby
return_to_scene(Scene_Map)
```

Exemple : Aller à l'écran titre
```ruby
return_to_scene(Scene_Title)
```

Note : Evitez de vous servir de la méthode `return_to_scene` pour revenir à une interface qui n'a pas appelé directement ou indirectement votre interface, cela pourrait provoquer de sérieux bugs.

## Appeler une interface

Il est également possible d'appeler une interface, pour ça utilisez la méthode `call_scene`. Elle prend les mêmes arguments que `return_to_scene` et arrête votre interface si jamais l'interface appelée à utilisé `return_to_scene` vers une des interfaces ayant appelé votre interface.

Exemple : Afficher le Pokédex
```ruby
call_scene(Dex)
```

Exemple : Afficher la page de Pikachu du Pokédex
```ruby
call_scene(Dex, 25)
```

## Mot de la fin

Avec tout ça, vous n'avez pas de quoi faire une interface qui fait grand chose mais vous avez les bases de `GamePlay::Base` pour coder une interface. Certaines notions de `GamePlay::Base` seront vues dans d'autres chapitres car ça s'y prêtera plus.